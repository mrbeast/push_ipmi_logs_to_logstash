#!/usr/bin/python
from __future__ import print_function
import time
import os
import re
import subprocess
from pprint import pprint
import threading
import Queue
import logging
import socket
import mmap
import fileinput

finding_ipmi_groups = subprocess.check_output('''tabdump ipmi | cut -d'"' -f2 | awk "{if (NR!=1) {print}}"''', shell=True, universal_newlines=True)
ipmi_groups = finding_ipmi_groups.rstrip().split("\n")
tmp_path = '/var/log/ipmi-logs-test'
NUM_WORKERS = 1
host = "10.252.206.12"
port = 10514
logging.basicConfig(filename='/var/log/ipmi-logs-test/error.log', format='%(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S')



#def worker_func(pending, done):
#for group in ipmi_groups:
#nodes_check = subprocess.check_output('''/opt/xcat/bin/pping -i ipmi {0} | awk '{{ if (($2=="ping")) {{print $1}} }}' | cut -d '-' -f1'''.format(group), shell=True, universal_newlines=True)

def tail(filename, n):
    with open(filename, 'r+') as f:
        try:
            f.seek(0, 2)
            endf = pos = f.tell()
            linecount = 0
            while pos >=0:
                f.seek(pos)
                next_char = f.read(1)
                if next_char == "\n" and pos != endf-1:
                    linecount += 1

                if linecount == n:
                    break
                pos -= 1

            if pos < 0:
                f.seek(0)

            s = f.read()
            return s
        except Exception:
            pass



for group in ipmi_groups:
    nodes_check = subprocess.check_output('''/opt/xcat/bin/pping -i ipmi {0} | awk '{{ if (($2=="ping")) {{print $1}} }}' | cut -d ':' -f1'''.format(group), shell=True, universal_newlines=True)
    nodes_list = nodes_check.rstrip().split("\n")
    for node in nodes_list:
        final_path_node = os.path.join(tmp_path, node)
        log_old_path = os.path.join(final_path_node, 'log.old')
        if not os.path.isdir(tmp_path):
            print(tmp_path + ' not exists, creating...')
            os.mkdir(tmp_path)

        if not os.path.isdir(final_path_node):
            print('Creating ' + final_path_node)
            os.mkdir(final_path_node)

        os.chdir(final_path_node)

        if not os.path.isfile(log_old_path) or os.path.getsize(log_old_path) == 0:
            print('Searching log.old...\nlog.old not found!\nSaving logs from ' + node + ' to log.old')
            try:
                #out = subprocess.Popen('''ipmiutil sel -N {0} -U ADMIN -P ADMIN | sed '1,7d' | sed '$d' > log.old'''.format(node), shell=True, stderr=subprocess.PIPE)
                proc = subprocess.Popen('''ipmiutil sel -N {0} -U ADMIN -P ADMIN | sed '1,7d' | sed '$d' '''.format(node), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = proc.communicate()
                with open('log.old', 'w') as f:
                    for line in out.splitlines():
                        f.write('{0} {1}\n'.format(node.rstrip('-ipmi'), line))

                if not "" == err:
                    print('REVENT Error while trying to pulling logs from ' + node + '\n' + err)
                    logging.warning('REVENT Error while trying to pulling logs from {0}'.format(node))
                    logging.warning('{0}'.format(err))
                    print('===================================================================')

                else:
                    try:
                        print('Pulling logs from ' + node + ' to rsyslog server')
                        with open('log.old', 'r') as f:
                            sock = socket.socket()
                            sock.connect((host, port))
                            for line in f:
                                sock.send(line)
                            sock.close()
                        print('===================================================================')
                    except IOError:
                        print('log.old not found.')
                        logging.warning('log.old not found.')
                        print('===================================================================')
            except Exception:
                print('SOCKET Error while trying to pulling logs from ' + node)
                print('===================================================================')

        else:
            print('Searching log.old...\nlog.old found!\nSaving logs from ' + node + ' to log.new')
            proc = subprocess.Popen('''ipmiutil sel -N {0} -U ADMIN -P ADMIN | sed '1,7d' | sed '$d' '''.format(node), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            opt, err = proc.communicate()
            with open('log.new', 'w') as f:
                for line in out.splitlines():
                    f.write('{0} {1}\n'.format(node.rstrip('-ipmi'), line))

            if not "" == err:
                print('REVENT Error while trying to pulling logs from ' + node + '\n' + err)
                logging.warning('REVENT Error while trying to pulling logs from {0}'.format(node))
                logging.warning('{0}'.format(err))
                print('===================================================================')

            else:
                print('Checking new logs for ' + node)

                p = tail('log.old', 5)
                r = re.escape(p)

                try:
                    with open('log.new', 'r+') as f:
                        data = mmap.mmap(f.fileno(), 0)
                        find_match = re.search(r, data)
                        #print(find_match.start(0))
                        #print(find_match.end(0))
                        m = find_match.end(0)
                        f.seek(0, 2)
                        end = f.tell()
                        if m == end :
                            print('No new logs for ' + node)
                            print('===================================================================')
                        else:
                            print('Match found! Copyig new logs to log.old')
                            f.seek(m)
                            s = f.read()
                            with open('log.old', 'w') as f:
                                for line in s:
                                    f.write(line)

                            try:
                                print('Pulling NEW logs from ' + node + ' to rsyslog server')
                                with open('log.old', 'r') as f:
                                    sock = socket.socket()
                                    sock.connect((host, port))
                                    for line in f:
                                        sock.send(line)
                                    sock.close()
                                print('===================================================================')
                            except IOError:
                                print('log.old not found.')
                                logging.warning('log.old not found')
                                print('===================================================================')

                except Exception as e:
                    print(e)
                    print('===================================================================')
                    pass

